var socket = io.connect('http://localhost:3000');

// socket.on('news', function(data){
//   console.log(data);
//   socket.emit('my other event', {my: 'data'});
// });

// socket.emit('set nickname', 'tanaka');

var $btn   = document.getElementById('talk');
var $apply = document.getElementById('apply');

$apply.addEventListener('click', function(e){
  e.preventDefault();
  var nickname = document.getElementById('nickname').value;
  socket.emit('set nickname', nickname);
});

$btn.addEventListener('click', function(e){
  e.preventDefault();
  var msg = document.getElementById('input_text').value;
  socket.emit('msg', msg);
}, false);

socket.on('update', function(data){
  console.log(data);
});

socket.on('ready', function(data){
  console.log('ready');
});

socket.on('connect', function(){
  console.log('connect!');
});

socket.on('nickname', function(){
  console.log('nickname!');
});

// socket.emit('set nickname', 'hoge');

