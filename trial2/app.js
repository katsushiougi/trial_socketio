
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// app.get('/', routes.index);
// app.get('/users', user.list);


var server = http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

// ソケット
var io = require('socket.io').listen(server);

// io.sockets.on('connection', function(socket){
//   socket.emit('news', {hello:'world'});
//   socket.on('my other event', function(data){
//     console.log(data);
//   });
// });

// io.sockets.on('connection', function(socket){

//   // カスタムイベント
//   io.sockets.emit('this', {will: 'be received by everyone'});

//   socket.on('private message', function(from, msg){
//     console.log('I received a private message by ', from, ' saying ', msg);
//   });

//   socket.on('disconnect', function(){
//     io.sockets.emit('user disconnected');
//   });

// });

// データをストアする

io.sockets.on('connection', function(socket){

  socket.emit('connect');

  socket.on('set nickname', function(name){
    // ニックネームをセット
    socket.set('nickname', name, function(){
      console.log(name);
      // socket.emit('ready');
      // 送信者以外の全員にエミット
      socket.broadcast.emit('ready');
    });
  });

  socket.on('msg', function(msg){

    socket.get('nickname', function(err, name){
      if (!name) return;
      console.log('Chat message by ', name);
      console.log(msg);
      socket.broadcast.emit('update', {nickname: name, msg:msg});
    });

  });

});